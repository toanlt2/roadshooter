using System;
using System.Collections;
using UnityEngine;

namespace SR4BlackDev.UI
{
    [RequireComponent(typeof(Animator))]
    [RequireComponent(typeof(CanvasGroup))]
    public class PopupAnimator : PopupAnimBase
    {
        [SerializeField] private Animator _animator;
        private float _openDuration;
        private float _closeDuration;
        private static readonly int open = Animator.StringToHash("Open");
        private static readonly int close = Animator.StringToHash("Close");

        private void Reset()
        {
            _animator = GetComponent<Animator>();
        }

        private void Awake()
        {
            if(!_animator) return;
            var clips = _animator.runtimeAnimatorController.animationClips;
            _openDuration = clips[0].length;
            _closeDuration = clips[1].length;
        }

        public override IEnumerator Open()
        {
            if(!_animator) yield break;
            _animator.SetTrigger(open);
            yield return new WaitForSeconds(_openDuration);
        }

        public override IEnumerator Close()
        {
            if(!_animator) yield break;
            _animator.SetTrigger(close);
            yield return new WaitForSeconds(_closeDuration);
        }
    }
}