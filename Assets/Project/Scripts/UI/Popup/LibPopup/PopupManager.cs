﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace SR4BlackDev.UI
{
    public class PopupManager : MonoBehaviour
    {
        [SerializeField] private RectTransform _layerBackground;
        [SerializeField] private RectTransform _layerMain;
        [SerializeField] private RectTransform _layerMenu;
        [SerializeField] private RectTransform _layerSub1;
        [SerializeField] private RectTransform _layerSub2;
        [SerializeField] private RectTransform _layerNotify;
        [SerializeField] private RectTransform _layerTutorial;
        [SerializeField] private RectTransform _layerOverlay;
        [SerializeField] private RectTransform _layerScreen;
        [SerializeField] private OverlayToast _toast;
        [SerializeField] private OverlayReward _reward;
        //[SerializeField] private ScreenTransition _transition;
        [SerializeField] private LayerGroup _layerGroup;
        [SerializeField] private GameObject _locker;
        public RectTransform LayerTutorial => _layerTutorial;
        public RectTransform LayerOverlay => _layerOverlay;
        
        private readonly Dictionary<LayerPopup, Queue<PopupData>> _popupQueue = new Dictionary<LayerPopup, Queue<PopupData>>();
        private readonly Dictionary<LayerPopup, PopupBase> _popupOpening = new Dictionary<LayerPopup, PopupBase>();
        private readonly Dictionary<string, PopupBase> _popupPool = new Dictionary<string, PopupBase>();
        private readonly List<LayerPopup> _popupBackList = new List<LayerPopup>();
        private static readonly LayerPopup[] PopupSequences = { LayerPopup.Notify };
        private static readonly LayerPopup[] PopupBackable = { LayerPopup.Sub1, LayerPopup.Sub2, LayerPopup.Notify };
        private const string PopupResourcePath = "Popups/";

        public Vector2 ConvertAnchorPos(RectTransform from) => _reward.ConvertAnchorPos(from);
        public RewardCell SpawnRewardCell() => _reward.Spawn();
        public void DespawnRewardCell(RewardCell cell) => _reward.Despawn(cell);
        public void ShowGroup() => _layerGroup.Show();
        public void HideGroup() => _layerGroup.Hide();
        public void LockScreen(bool locked) => _locker.SetActive(locked);
        public void ShowToast(string message) => _toast.ShowToast(message);
        //public void ChangeScene(string sceneName, bool isEntering) => _transition.LoadScene(sceneName, isEntering);

        public void OpenPopup(string popupName, LayerPopup layerPopup, Action<PopupBase> onOpen, Action<PopupBase> onClose)
        {
            if (!_popupQueue.TryGetValue(layerPopup, out var queue))
                _popupQueue.Add(layerPopup, queue = new Queue<PopupData>());
            queue.Enqueue(new PopupData(){Name = popupName, Layer = layerPopup, OnOpen = onOpen, OnClose = onClose});
            if(!_popupOpening.ContainsKey(layerPopup))
                OpenPopup(layerPopup);
            else if(!IsPopupSequence(layerPopup))
                ClosePopup(layerPopup);
        }

        private void OpenPopup(LayerPopup layerPopup)
        {
            if(!HasPopupInQueue(layerPopup)) return;
            DoOpen(_popupQueue[layerPopup].Dequeue());
            if (_popupQueue[layerPopup].Count == 0) 
                _popupQueue.Remove(layerPopup);
        }
        
        private void DoOpen(PopupData popupData)
        {
            if (!LoadPopup(popupData.Name, popupData.Layer, out var popupBase)) return;
            popupBase.transform.parent = GetLayerRoot(popupData.Layer);
            popupBase.transform.SetAsLastSibling();
            popupBase.Open(popupData);
            _popupOpening.Add(popupData.Layer, popupBase);
            if(IsBackable(popupData.Layer)) _popupBackList.Add(popupData.Layer);
        }
        
        public void ClosePopup(LayerPopup layerPopup)
        {
            if(!IsLayerOpening(layerPopup)) return;
            if(!_popupOpening[layerPopup].CanClose) return;
            DoClose(layerPopup);
            if (layerPopup != LayerPopup.Main) return;
            if(IsLayerOpening(LayerPopup.Sub1)) DoClose(LayerPopup.Sub1);
            if(IsLayerOpening(LayerPopup.Sub2)) DoClose(LayerPopup.Sub2);
        }

        private void DoClose(LayerPopup layerPopup)
        {
            _popupOpening[layerPopup].DoClose();
            _popupOpening.Remove(layerPopup);
            if(IsBackable(layerPopup)) _popupBackList.Remove(layerPopup);
        }

        private void DoBack()
        {
            if(_popupBackList.Count == 0) return;
            ClosePopup(_popupBackList[_popupBackList.Count - 1]);
        }

        public void OpenStart(LayerPopup layerPopup)
        {
        }

        public void OpenFinish(LayerPopup layerPopup)
        {
        }
        
        public void CloseStart(LayerPopup layerPopup)
        {
            if(IsPopupSequence(layerPopup)) return;
            OpenPopup(layerPopup);
        }

        public void CloseFinish(LayerPopup layerPopup)
        {
            if(!IsPopupSequence(layerPopup)) return;
            OpenPopup(layerPopup);
        }

        public bool LoadPopup(string popupName, LayerPopup layerPopup, out PopupBase popupBase)
        {
            if (_popupPool.ContainsKey(popupName))
            {
                popupBase = _popupPool[popupName];
                return true;
            }
            var prefab = Resources.Load<PopupBase>(PopupResourcePath + popupName);
            if (prefab != null)
            {
                var popup = Instantiate(prefab, GetLayerRoot(layerPopup));
                popup.Init();
                popup.gameObject.SetActive(false);
                _popupPool.Add(popupName, popup);
                popupBase = popup;
                return true;
            }
            popupBase = null;
            Debug.LogError("Missing popup prefab: " + PopupResourcePath + popupName);
            return false;
        }

        public bool GetPopup(LayerPopup layerPopup, out PopupBase popupBase)
        {
            if (_popupOpening.ContainsKey(layerPopup))
            {
                popupBase = _popupOpening[layerPopup];
                return true;
            }
            popupBase = null;
            return false;
        }

        public void ClearPopup()
        {
            foreach (var popup in _popupOpening)
                Destroy(popup.Value.gameObject);
            foreach (var popup in _popupPool)
                Destroy(popup.Value.gameObject);

            _reward.Clear();
            _popupPool.Clear();
            _popupQueue.Clear();
            _popupOpening.Clear();
            _popupBackList.Clear();
        }
        
        public bool IsLayerOpening(LayerPopup layerPopup)
        {
            return _popupOpening.ContainsKey(layerPopup);
        }

        public bool HasPopupInQueue(LayerPopup layerPopup)
        {
            return _popupQueue.ContainsKey(layerPopup);
        }

        private void Update()
        {
#if ENABLE_INPUT_SYSTEM
            if (UnityEngine.InputSystem.Keyboard.current.escapeKey.wasPressedThisFrame)
                DoBack();
#else
            if (Input.GetKeyDown(KeyCode.Escape))
                DoBack();
#endif
        }
        
        private bool IsPopupSequence(LayerPopup layerPopup)
        {
            for (int i = 0; i < PopupSequences.Length; i++)
                if (PopupSequences[i] == layerPopup)
                    return true;
            return false;
        }

        private bool IsBackable(LayerPopup layerPopup)
        {
            for (int i = 0; i < PopupBackable.Length; i++)
                if (PopupBackable[i] == layerPopup)
                    return true;
            return false;
        }

        private RectTransform GetLayerRoot(LayerPopup layer)
        {
            switch (layer)
            {
                case LayerPopup.Background:
                    return _layerBackground;
                case LayerPopup.Menu:
                    return _layerMenu;
                case LayerPopup.Main:
                    return _layerMain;
                case LayerPopup.Sub1:
                    return _layerSub1;
                case LayerPopup.Sub2:
                    return _layerSub2;
                case LayerPopup.Notify:
                    return _layerNotify;
                case LayerPopup.Tutorial:
                    return _layerTutorial;
                case LayerPopup.Overlay:
                    return _layerOverlay;
                case LayerPopup.Screen:
                    return _layerScreen;
                default:
                    return _layerNotify;
            }
        }
        void Awake()
        {
            if (instance == null)
                instance = this;
        }
        private static PopupManager instance;
        
        public static PopupManager Popup
        {
            get
            {
                if (instance == null)
                {
                    GameObject prefab = Resources.Load<GameObject>("Popups/PopupManager");
                    GameObject temp = Instantiate(prefab);
                    instance = temp.GetComponent<PopupManager>();
                }
                return instance;
            }
        }

        public static void Open(string popupName, LayerPopup layerPopup, Action<PopupBase> onOpen,
            Action<PopupBase> onClose)
        {
            Popup.OpenPopup(popupName,layerPopup,onOpen, onClose);
        }
    }
}