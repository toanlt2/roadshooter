﻿using TMPro;
using UnityEngine;

namespace SR4BlackDev.UI
{
    [RequireComponent(typeof(Animator))]
    public class ToastPopup : MonoBehaviour
    {
        [SerializeField] private Animator _animator;
        [SerializeField] private TextMeshProUGUI _message;
        private static readonly int open = Animator.StringToHash("Open");
        private void Reset() => _animator = GetComponent<Animator>();
        
        public void Show(string message)
        {
            _message.text = message;
            _animator.SetTrigger(open);
        }
    }
}