using System.Collections;
using UnityEngine;

namespace SR4BlackDev.UI
{
    public class PopupAnimBase : MonoBehaviour
    {
        public virtual IEnumerator Open()
        {
            yield return null;
        }

        public virtual IEnumerator Close()
        {
            yield return null;
        }
    }
}