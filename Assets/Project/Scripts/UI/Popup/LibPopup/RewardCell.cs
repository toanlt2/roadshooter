using UnityEngine;
using UnityEngine.UI;

namespace SR4BlackDev.UI
{
    public class RewardCell : MonoBehaviour
    {
        [SerializeField] private Image _icon;
        [SerializeField] private RectTransform _rect;
        public RectTransform Rect => _rect;
        public int Id { get; set; }

        public void SetIcon(Sprite icon)
        {
            _icon.sprite = icon;
        }
    }
}