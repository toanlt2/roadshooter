using System;
using System.Collections;
using System.Collections.Generic;
using SR4BlackDev.UI;
using UnityEditor;
using UnityEngine;

public class TestOpenPopup : MonoBehaviour
{
    public void OpenPopup1()
    {
        PopupManager.Open("Popup1", LayerPopup.Main,null, null);
    }
    public void OpenPopup2()
    {
        PopupManager.Open("Popup2", LayerPopup.Main,null, null);
    }
}
[CustomEditor(typeof(TestOpenPopup))]
public class CustomTestOpenPopup : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        TestOpenPopup yourClass = (TestOpenPopup)target;

        if (GUILayout.Button("Button 1"))
        {
            yourClass.OpenPopup1();
        }

        if (GUILayout.Button("Button 2"))
        {
            yourClass.OpenPopup2();
        }
    }
}
