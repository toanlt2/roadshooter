using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class PreviewAnimation : MonoBehaviour
{
    public Animator animator;
    public Vector3 getForwardVector;
    public Transform parent;
    public Transform forwardVector;
    public float angle;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public string nameClip;
    public void PlayAnimation()
    {
        animator.Play(nameClip,0,0);
    }

    public void CrossFadeAnim()
    {
        animator.CrossFade(nameClip,.25f,0);
    }
    
    public void GetForward()
    {
        Vector3 baseForward = parent.transform.forward;
        
        Quaternion lookRotation = Quaternion.FromToRotation(Vector3.forward, baseForward);;
        Vector3 realDirection = lookRotation * forwardVector.forward; 
        getForwardVector = realDirection.normalized;
    }
    
    //Event Animation
    public void StandUpFinish(){}
}
[CustomEditor(typeof(PreviewAnimation))] // Thay "YourClass" bằng tên của lớp bạn muốn tạo custom editor
public class PreviewAnimationEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        PreviewAnimation yourClass = (PreviewAnimation)target;

        if (GUILayout.Button("PlayAnim"))
        {
            yourClass.PlayAnimation();
        }
        if (GUILayout.Button("CrossFadeAnim"))
        {
            yourClass.CrossFadeAnim();
        }

        if (GUILayout.Button("Get Forward"))
        {
            yourClass.GetForward();
        }
    }
}