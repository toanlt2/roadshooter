using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    private static CameraController _instance;

    public static CameraController instance
    {
        get { return _instance; }   
    }
    

    public Camera cam;

    private void Awake()
    {
        _instance = this;
    }

    public static Transform GetCamera
    {
        get { return instance.cam.transform; }
    }
}
