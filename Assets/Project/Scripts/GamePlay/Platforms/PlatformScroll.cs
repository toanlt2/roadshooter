using System;
using System.Collections;
using System.Collections.Generic;
using SR4BlackDev;
using UnityEngine;

public class PlatformScroll : MonoBehaviour
{
    public List<GameObject> prefabs = new List<GameObject>();

    public List<GameObject> spawned = new List<GameObject>();
    public Transform playerTrans;

    [SerializeField] private int current = 0;
    [SerializeField] private float stepSpawn = 100f;
    [SerializeField] private int maxSize = 5;
    [SerializeField] private int recycleIndex = 0;

    private int[] arrayRandomPrefab;
    private void Awake()
    {
        Setup();
    }

    private void Setup()
    {
        arrayRandomPrefab = new int[100];
        
        for (int i = 0; i < 100; i++)
        {
            arrayRandomPrefab[i] = UnityEngine.Random.Range(0, prefabs.Count);
        }

        for (int i = 0; i < prefabs.Count; i++)
        {
            PoolManager.Create(prefabs[i]);
        }
    }
    

    private void Start()
    {
        
        SpawnPlatform();
    }

    private void FixedUpdate()
    {
        if (playerTrans.position.z > current * stepSpawn )
        {
            current += 1;
            SpawnPlatform();
        }
        
    }

    void SpawnPlatform()
    {
        int getIndexPrefab = arrayRandomPrefab[current%arrayRandomPrefab.Length];
        GameObject a = PoolManager.Spawn(prefabs[getIndexPrefab]);
        spawned.Add(a);
        a.transform.position = new Vector3(0,0,current * stepSpawn);
        a.SetActive(true);
        if (current - recycleIndex > maxSize)
        {
            PoolManager.Recycle(spawned[recycleIndex].GetInstanceID());
            recycleIndex += 1;
        }
    }

    void StartSpawn()
    {
        
    }
}
