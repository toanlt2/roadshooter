using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using SR4BlackDev;
using UnityEngine;

public class EnemyCar : ICar
{
    [SerializeField] private ICharacter prefabCharacter;
    [SerializeField] private ICharacter prefabDriver;
    [SerializeField] private List<Transform> otherTrans;
    
    private List<ICharacter> characters = new List<ICharacter>();
    private ICharacter driveCharacter;
    
    
    
    [SerializeField] private bool isPlay = false;
    [SerializeField] private float speedMove;
    [SerializeField] private float currentSpeed;
    [SerializeField] private Vector3 refRoad;
    [SerializeField] private float maxSpeedMovement;
    [SerializeField] private Rigidbody rgBody;
    
    private void SpawnCharacter()
    {
        for (int i = 0; i < otherTrans.Count; i++)
        {
            ICharacter temp;
            if(i==0)
                temp = PoolManager.Spawn(prefabDriver.gameObject).GetComponent<ICharacter>();
            else
            {
                temp = PoolManager.Spawn(prefabCharacter.gameObject).GetComponent<ICharacter>();
            }
            temp.gameObject.SetActive(true);
            temp.transform.parent = otherTrans[i].transform;
            temp.transform.localPosition = Vector3.zero;
            temp.transform.DORotate(Vector3.zero , 0f);
            characters.Add(temp);
        }

        driveCharacter = characters[0];
    }

    private void Awake()
    {
        SpawnCharacter();
    }

    private void Start()
    {

    }

    public override void UpdateCar()
    {
        base.UpdateCar();
        if (!isPlay) return;
        Move();
    }

    public float angleBack = 20f;
    public float currentAngle;
    public override void Move()
    {
//        Vector3 forward = transform.forward;
//        forward.y = 0;
//        currentAngle = Vector3.Angle(forward, Vector3.forward);
//
//        Vector3 velocity = Vector3.zero;
//        if (Mathf.Abs(currentAngle) > angleBack)
//        {
//            velocity.x = -forward.x;
//            velocity.z = currentSpeed;
//        }
//        else
//        {
//            velocity = rgBody.velocity;
//        }
        Vector3 velocity = Vector3.zero;
        velocity = rgBody.velocity;
        rgBody.velocity = new Vector3(velocity.x, velocity.y, currentSpeed);
        
        
        
    }

    public override void StartGamePlay()
    {
        base.StartGamePlay();
        for (int i = 0; i < characters.Count; i++)
        {
            characters[i].OnCover();
            characters[i].transform.localPosition = Vector3.zero;
        }
        DOVirtual.Float(0, speedMove, .5f, delegate(float value) { currentSpeed = value; });
        isPlay = true;
    }

    public override void EndGamePlay()
    {
        base.EndGamePlay();
        isPlay = false;
    }
}
#if UNITY_EDITOR
[UnityEditor.CustomEditor(typeof(EnemyCar))] 
public class EnemyCarEditor : UnityEditor.Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        EnemyCar yourClass = (EnemyCar)target;
        
        if (GUILayout.Button("OnCreate"))
        {
            yourClass.OnCreate();
        }
        
        if (GUILayout.Button("StartGamePlay"))
        {
            yourClass.StartGamePlay();
        }

        if (GUILayout.Button("EndGamePlay"))
        {
            yourClass.EndGamePlay();
        }
        
        if (GUILayout.Button("Preview"))
        {
            yourClass.Preview();
        }
    }
}
#endif