using System;
using System.Collections;
using System.Collections.Generic;
using SR4BlackDev;
using UnityEngine;

public class WheelMovement : MonoBehaviour
{
    
    public WheelCollider frontLeft,frontRight,backLeft,backRight;
    public float motorTorque = 1000f;
    public float steeringAngle = 30f;

    public float force= 1f;
    public Vector3 forceDirFrontLeft, forceDirFrontRight;

    [SerializeField] private ICar car;
    private void FixedUpdate()
    {
        float motor = Input.GetAxis("Vertical") * motorTorque;
        float steering = Input.GetAxis("Horizontal") * steeringAngle;

        // Gán giá trị điều khiển cho Wheel Collider
        frontLeft.motorTorque = motor;
        frontRight.motorTorque = motor;

        frontLeft.steerAngle = steering;
        frontRight.steerAngle = steering;
    }
    public void AttackLeftWheel()
    {
        
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawLine(backRight.transform.position , backRight.transform.position + forceDirFrontLeft * force );
        Gizmos.DrawLine(backLeft.transform.position , backLeft.transform.position + forceDirFrontRight * force );
    }

    public void OnAttackWheelFrontLeft()
    {
        backLeft.attachedRigidbody.AddForceAtPosition(forceDirFrontLeft * force , backRight.transform.position );
        car.EndGamePlay();
    }
    public void OnAttackWheelFrontRight()
    {
        backRight.attachedRigidbody.AddForceAtPosition(forceDirFrontRight * force , backLeft.transform.position );
        car.EndGamePlay();
    }
}
#if UNITY_EDITOR
[UnityEditor.CustomEditor(typeof(WheelMovement))] 
public class WheelMovementEditor : UnityEditor.Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        WheelMovement yourClass = (WheelMovement)target;
        
        if (GUILayout.Button("AddForce"))
        {
            yourClass.AttackLeftWheel();
        }
                
        if (GUILayout.Button("Attack Wheel Front Left"))
        {
            yourClass.OnAttackWheelFrontLeft();
        }

                
        if (GUILayout.Button("Attack Wheel Front Right"))
        {
            yourClass.OnAttackWheelFrontRight();
        }
    }
}
#endif