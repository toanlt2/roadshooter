using System;
using System.Collections;
using System.Collections.Generic;
using SR4BlackDev;
using UnityEngine;

namespace SR4BlackDev
{
    public class PlayerCharacter : ICharacter
    {
        [SerializeField] private string shotAnim;
        [SerializeField] private string coverAnim;
        private void Start()
        {
            this.PostEvent(EventID.ADD_CHARACTER, this);
        }

        public override void OnShoot()
        {
            base.OnShoot();
            PlayAnim(shotAnim);
        }

        public override void OnCover()
        {
            base.OnCover();
            PlayAnim(coverAnim,0f);  
        }
    }


}
