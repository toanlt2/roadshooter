using System.Collections;
using System.Collections.Generic;
using SR4BlackDev;
using UnityEngine;

namespace SR4BlackDev
{
    public class DefaultCharacter : ICharacter
    {
        [SerializeField] private string shotAnim;
        [SerializeField] private string coverAnim;
        private void Start()
        {
            this.PostEvent(EventID.ADD_CHARACTER, this);
        }

        public override void OnShoot()
        {
            base.OnShoot();
            PlayAnim(shotAnim);
        }

        public override void OnCover()
        {
            base.OnCover();
            PlayAnim(coverAnim,0f);  
        }
    }
#if UNITY_EDITOR
    [UnityEditor.CustomEditor(typeof(DefaultCharacter))] 
    public class DefaultCharacterEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            DefaultCharacter yourClass = (DefaultCharacter)target;
        
            if (GUILayout.Button("OnShoot"))
            {
                yourClass.OnShoot();
            }
        
            if (GUILayout.Button("OnCover"))
            {
                yourClass.OnCover();
            }

        }
    }
#endif
}
