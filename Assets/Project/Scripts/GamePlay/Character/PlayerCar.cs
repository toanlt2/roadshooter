using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Serialization;

namespace SR4BlackDev
{
    public class PlayerCar : ICar
{

    [SerializeField] private string idleAnim;
    [SerializeField] private string jumpingUp;
    [SerializeField] private string jumpingDown;
    
    //Target
    [SerializeField] private Transform defaultTrans;
    [SerializeField] private Transform coverTrans;
    [SerializeField] private Transform aimTrans; // thay = target position 
    [SerializeField] private Transform coverAimTrans;

    
    //Preview
    [SerializeField] private Transform previewTrans;
    [SerializeField] private float rotationSpeed;
    [SerializeField] private Vector3 localGunForward;

    //View Camera
    [SerializeField] private Transform topView;
    [SerializeField] private Transform previewView;

    //Curve
    [SerializeField] private AnimationCurve curveJumpingUp;
    [SerializeField] private AnimationCurve curveJumpDown;

    //Current Weapon
    [SerializeField] private IWeapon currentWeapon;
    
    //Current Character
    [SerializeField] private ICharacter currentCharacter;
    
    //Current Car
    public Rigidbody rgBody;
    //Car State
    [SerializeField] private bool isPlay = false;
    [SerializeField] private bool isShooting = false;
    [SerializeField] private Vector3 aimPosition;
    [SerializeField] private float speedMove;
    [SerializeField] private float currentSpeed;
    [SerializeField] private Vector3 refRoad;
    [SerializeField] private float maxSpeedMovement;
    
    private void Start()
    {
        Preview();
    }

    public override void OnCreate()
    {
        base.OnCreate();
        entity.AddPlayerFlag();
    }

    public override void StartGamePlay()
    {
        base.StartGamePlay();

        Transform trans = currentCharacter.transform;
        currentCharacter.PlayAnim(jumpingUp,0f);
        trans.DOLocalMove(defaultTrans.localPosition, .15f).OnComplete(delegate
        {
            trans.DOLocalMove(coverTrans.localPosition,.25f).OnComplete(delegate
            {
                isPlay = true;
                currentCharacter.OnCover();
            }).SetEase(curveJumpingUp).OnUpdate(delegate
            {
                Vector3 directionToTarget = coverAimTrans.position - trans.position;
                directionToTarget.y = 0f; 
                Quaternion targetRotation = Quaternion.LookRotation(directionToTarget);
                trans.rotation = Quaternion.RotateTowards(trans.rotation, targetRotation, Time.fixedDeltaTime * rotationSpeed);
            });
            DOVirtual.Float(0, speedMove, .5f, delegate(float value) { currentSpeed = value; });
        });
        
        
        
        
        
        
        DOVirtual.DelayedCall(.15f, delegate { trans.DORotate(Vector3.zero, .25f); });
        
        Transform cameraTrans = CameraController.GetCamera;
        cameraTrans.parent = topView;
        cameraTrans.DOLocalMove(Vector3.zero, 1f);
        cameraTrans.DORotate(Vector3.zero + new Vector3(0,180f,0), 1f);
    }

    public override void Preview()
    {
        base.Preview();
        Transform trans = currentCharacter.transform;
        currentCharacter.PlayAnim(idleAnim);
        trans.localPosition = previewTrans.localPosition;
        trans.forward = previewTrans.forward;
        
        Transform cameraTrans = CameraController.GetCamera;
        cameraTrans.parent = previewView;
        //cameraTrans.localPosition = Vector3.zero;
        cameraTrans.DOLocalMove(Vector3.zero, 1f);
        //cameraTrans.forward = previewView.forward;
        cameraTrans.DORotate(previewView.localRotation.eulerAngles, 1f);
    }

    public override void EndGamePlay()
    {
        base.EndGamePlay();
        isPlay = false;
        Transform trans = currentCharacter.transform;
        currentCharacter.PlayAnim(jumpingDown);
        trans.localPosition = coverTrans.localPosition;
        trans.DOLocalMove(previewTrans.localPosition, 0.25f).SetEase(curveJumpDown).OnComplete(delegate
        {
            Preview();
        });
        currentSpeed = 0;
    }
    
    void Update()
    {
        if (!isPlay) return;
        if (!currentWeapon) return;
        if (Input.GetKey(KeyCode.Space))
        {
            if(currentWeapon.CanShoot() && !isShooting)
                Shoot();
        }
        
        if (Input.GetKeyUp(KeyCode.Space))
        {
            Cover(); 
        }
    }

    public override void UpdateCar()
    {
        if (!isPlay) return;
        if (!currentWeapon) return;
        Move();
        aimPosition = aimTrans.position;
        
        if (!currentWeapon.CanShoot())
        {
            currentWeapon.Reloading();
                
            Vector3 directionToTarget = coverAimTrans.position - currentCharacter. transform.position;
            directionToTarget.y = 0f; 
            Quaternion targetRotation = Quaternion.LookRotation(directionToTarget);
            currentCharacter. transform.rotation = Quaternion.RotateTowards(currentCharacter. transform.rotation, targetRotation, Time.fixedDeltaTime * rotationSpeed);
            currentWeapon.EndShoot();
                
            Cover();
            return;
        }
        
        
        if (isShooting )
        {

            Vector3 directionToTarget = (aimPosition - currentCharacter. transform.position).normalized;
            directionToTarget.y = 0;
            Vector3 baseForard = directionToTarget;

            Quaternion lookRotation = Quaternion.FromToRotation(Vector3.forward, baseForard);;
            Vector3 realDirection = lookRotation * localGunForward;

            realDirection.y = 0;
            currentCharacter.transform.forward =  realDirection;
            currentWeapon.UpdateShoot(aimPosition);
        }
        else
        {
            Vector3 directionToTarget = coverAimTrans.position - currentCharacter. transform.position;
            directionToTarget.y = 0f; 
            Quaternion targetRotation = Quaternion.LookRotation(directionToTarget);
            currentCharacter. transform.rotation = Quaternion.RotateTowards(currentCharacter. transform.rotation, targetRotation, Time.fixedDeltaTime * rotationSpeed);

            currentWeapon.EndShoot();
            
        }

    }
    
    public void Shoot()
    {
        currentCharacter.OnShoot();
        currentCharacter.transform.DOLocalMove(defaultTrans.localPosition,0.25f);
        isShooting = true;
    }

    public void Cover()
    {
        currentCharacter.OnCover();
        currentCharacter.transform.DOLocalMove(coverTrans.localPosition,0.25f);
        isShooting = false;

    }

    
    
    public override void Move()
    {
        base.Move();
        transform.position = new Vector3(refRoad.x, transform.position.y, transform.position.z);
        Vector3 velocity = rgBody.velocity;
        rgBody.velocity = new Vector3(velocity.x, velocity.y, velocity.z > maxSpeedMovement ? maxSpeedMovement : velocity.z);
        rgBody.AddForce(Vector3.forward * currentSpeed);
        
        
    }
}
#if UNITY_EDITOR
    [UnityEditor.CustomEditor(typeof(PlayerCar))] 
    public class PlayerCarEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            PlayerCar yourClass = (PlayerCar)target;
        
            if (GUILayout.Button("OnCreate"))
            {
                yourClass.OnCreate();
            }
        
            if (GUILayout.Button("StartGamePlay"))
            {
                yourClass.StartGamePlay();
            }

            if (GUILayout.Button("EndGamePlay"))
            {
                yourClass.EndGamePlay();
            }
        
            if (GUILayout.Button("Preview"))
            {
                yourClass.Preview();
            }
        }
    }
#endif
}


