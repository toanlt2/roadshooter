using System.Collections;
using System.Collections.Generic;
using Entitas;
using Entitas.Unity;
using UnityEngine;

namespace SR4BlackDev
{
    public class ICar : MonoBehaviour
    {
        protected Entity entity;
        public virtual void OnCreate()
        {
            if (entity != null)
            {
                this.PostEvent(EventID.DESTROY_ENTITY, entity);
                gameObject.Unlink();
            }
            entity = Contexts.SharedInstance.game.CreateEntity(gameObject);
            gameObject.Link(entity);
            
            entity.AddCar(new ICarComponent(){car = this});
        }
        public virtual void Preview(){}
        public virtual void StartGamePlay(){}
        public virtual void EndGamePlay(){}
        public virtual void Move(){}
        
        public virtual void UpdateCar(){}
        
    }
}


