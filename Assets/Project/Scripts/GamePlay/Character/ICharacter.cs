using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SR4BlackDev
{
    public class ICharacter : MonoBehaviour
    {
        public Animator animator;
        public virtual void OnHit(){}
        public virtual void OnShoot(){}
        public virtual void OnDeath(){}
        public virtual void OnCover(){}
        public virtual void OnPreview(){}

        public void PlayAnim(string nameAnim)
        {
            animator.CrossFade(nameAnim,.25f,0);
        }

        public void PlayAnim(string nameAnim, float timeTransition)
        {
            animator.CrossFade(nameAnim,timeTransition,0);
        }
    }


}
