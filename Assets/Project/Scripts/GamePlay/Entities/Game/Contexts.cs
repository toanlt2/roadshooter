namespace SR4BlackDev
{

    public partial class Contexts : Entitas.IContexts {

        public static Contexts SharedInstance {
            get {
                if (_sharedInstance == null) {
                    _sharedInstance = new Contexts();
                }

                return _sharedInstance;
            }
            set { _sharedInstance = value; }
        }
        
        static Contexts _sharedInstance;

        public GameContext game { get; set; }

        public Entitas.IContext[] allContexts { get { return new Entitas.IContext [] { game }; } }

        public Contexts() {
            game = new GameContext();

            var postConstructors = System.Linq.Enumerable.Where(
                GetType().GetMethods(),
                method => System.Attribute.IsDefined(method, typeof(Entitas.CodeGeneration.Attributes.PostConstructorAttribute))
            );

            foreach (var postConstructor in postConstructors) {
                postConstructor.Invoke(this, null);
            }
        }

        public void Reset() {
            var contexts = allContexts;
            for (int i = 0; i < contexts.Length; i++) {
                contexts[i].Reset();
            }
        }
    }

    //------------------------------------------------------------------------------
    public partial class Contexts {

    #if (!ENTITAS_DISABLE_VISUAL_DEBUGGING && UNITY_EDITOR)

        [Entitas.CodeGeneration.Attributes.PostConstructor]
        public void InitializeContextObservers() {
            try {
                CreateContextObserver(game);
            } catch(System.Exception) {
            }
        }

        public void CreateContextObserver(Entitas.IContext context) {
            if (UnityEngine.Application.isPlaying) {
                var observer = new Entitas.VisualDebugging.Unity.ContextObserver(context);
                UnityEngine.Object.DontDestroyOnLoad(observer.gameObject);
            }
        }

    #endif
    }
    

}