using System.Collections.Generic;
using Entitas;
using UnityEngine;

namespace SR4BlackDev
{
    public sealed partial class GameContext : Context<Entity> {

        public GameContext()
            : base(
                GameComponentsLookup.TotalComponents,
                0,
                new ContextInfo(
                    "Game",
                    GameComponentsLookup.componentNames,
                    GameComponentsLookup.componentTypes
                ),
                (entity) =>

    #if (ENTITAS_FAST_AND_UNSAFE)
                    new Entitas.UnsafeAERC(),
    #else
                    new Entitas.SafeAERC(entity),
    #endif
                () => new Entity()
            ) {
        }

        public Dictionary<int, Entity> entitiesDictionary = new Dictionary<int, Entity>();

        public Entity CreateEntity(GameObject gameObject)
        {
            return CreateEntity(gameObject.GetInstanceID());
        }
        public Entity CreateEntity(int instanceId)
        {
            Entity newEntity = CreateEntity();
            if (entitiesDictionary.ContainsKey(instanceId))
            {
                entitiesDictionary[instanceId] = newEntity;
            }
            else
            {
                entitiesDictionary.Add(instanceId, newEntity);   
            }
            return newEntity;
        }

        public Entity GetEntity(int instanceId)
        {
            if (!entitiesDictionary.ContainsKey(instanceId)) return null;
            
            return entitiesDictionary[instanceId];
        }
        public Entity GetEntity(GameObject gameObject)
        {
            return GetEntity(gameObject.GetInstanceID());
        }
    }
}