namespace SR4BlackDev
{
    public static class GameComponentsLookup 
    {
        public const int ICarComponent = 0;
        public const int IPlayerFlag = 1;
        
        public static int TotalComponents
        {
          get { return componentTypes.Length; }   
        }

        public static readonly string[] componentNames = {
            "ICarComponent",
            "IPlayerFlag",
        };

        public static readonly System.Type[] componentTypes = {
            typeof(ICarComponent),
            typeof(IPlayerFlag),
        };
    }
}