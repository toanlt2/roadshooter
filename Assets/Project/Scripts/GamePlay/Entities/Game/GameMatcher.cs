using Entitas;

namespace SR4BlackDev
{
    public sealed partial class GameMatcher {

        public static Entitas.IAllOfMatcher<Entity> AllOf(params int[] indices) {
            return Entitas.Matcher<Entity>.AllOf(indices);
        }

        public static Entitas.IAllOfMatcher<Entity> AllOf(params Entitas.IMatcher<Entity>[] matchers) {
              return Entitas.Matcher<Entity>.AllOf(matchers);
        }

        public static Entitas.IAnyOfMatcher<Entity> AnyOf(params int[] indices) {
              return Entitas.Matcher<Entity>.AnyOf(indices);
        }

        public static Entitas.IAnyOfMatcher<Entity> AnyOf(params Entitas.IMatcher<Entity>[] matchers) {
              return Entitas.Matcher<Entity>.AnyOf(matchers);
        }
    }
}