using System.Collections;
using System.Collections.Generic;
using Entitas;
using SR4BlackDev;
using UnityEngine;

namespace SR4BlackDev
{
    public static class EntitiesExtension 
    {
        public static void AddCar(this Entity entity, ICarComponent car)
        {
            int index = GameComponentsLookup.ICarComponent;
            entity.AddComponent(index, car);
        }

        public static void AddPlayerFlag(this Entity entity)
        {
            int index = GameComponentsLookup.IPlayerFlag;
            entity.AddComponent(index, new IPlayerFlag());
        }
    }

    public static class EntitiesGetExtension
    {
        private static T Get<T>(this Entity entity, int index) where T : IComponent
        {
            return (T) entity.GetComponent(index);
        }
        public static ICarComponent GetCar(this Entity entity)
        {
            int index = GameComponentsLookup.ICarComponent;
            return entity.Get<ICarComponent>(index);
        }
        
    }
}
