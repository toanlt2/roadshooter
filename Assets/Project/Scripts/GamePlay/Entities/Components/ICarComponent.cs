using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Entitas;
namespace SR4BlackDev
{
    public class ICarComponent : IComponent
    {
        public ICar car;
    }
    
    public sealed partial class GameMatcher {

        static IMatcher<Entity> _iCar;

        public static IMatcher<Entity> ICar {
            get {
                if (_iCar == null) {
                    var matcher = (Matcher<Entity>)Matcher<Entity>.AllOf(GameComponentsLookup.ICarComponent);
                    matcher.componentNames = GameComponentsLookup.componentNames;
                    _iCar = matcher;
                }
                return _iCar;
            }
        }
    }
    


}
