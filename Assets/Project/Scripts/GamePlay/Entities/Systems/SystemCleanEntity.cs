using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Entitas;
namespace SR4BlackDev
{
    public class SystemCleanEntity : ICleanupSystem
    {
        private List<IEntity> entities;
        public SystemCleanEntity()
        {
            this.RegisterListener(EventID.DESTROY_ENTITY, CallBack);
            entities = new List<IEntity>();
        }

        private void CallBack(object arg1, object arg2)
        {
            IEntity entity = arg2 as IEntity;
            entities.Add(entity);
        }

        public void Cleanup()
        {
            for (int i = 0; i < entities.Count; i++)
            {
                entities[i].Destroy();
            }
            entities.Clear();
        }
    }


}
