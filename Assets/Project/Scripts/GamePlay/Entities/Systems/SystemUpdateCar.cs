using System.Collections;
using System.Collections.Generic;
using Entitas;
using SR4BlackDev;
using UnityEngine;

namespace SR4BlackDev
{
    
    public class SystemUpdateCar : IExecuteSystem
    {
        private readonly IGroup<Entity> _entities;
        public SystemUpdateCar(GameContext context)
        {
            _entities = context.GetGroup(GameMatcher.ICar);
        }
        public void Execute()
        {
            foreach (var entity in _entities)
            {
                entity.GetCar().car.UpdateCar();
            }
        }
    }

}
