using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IWeapon : MonoBehaviour
{
    [SerializeField] private int power;
    [SerializeField] private int ammo;
    [SerializeField] private float reload;
    public virtual void UpdateShoot(Vector3 targetPosition)
    {
        
    }

    public virtual void EndShoot()
    {
        
    }

    public virtual bool CanShoot()
    {
        return true;
    }

    public virtual void Reloading()
    {
        
    }
}
