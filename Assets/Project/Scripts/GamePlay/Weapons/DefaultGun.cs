using System.Collections;
using System.Collections.Generic;
using System.Timers;
using DG.Tweening;
using SR4BlackDev;
using UnityEngine;

public class DefaultGun : IWeapon
{

    [SerializeField] private ParticleSystem effect;
    
    [SerializeField] private float timeWait;
    
    [SerializeField] private float timeReload;

    [SerializeField] private int currentBullet;
    
    [SerializeField] private int maxBullet;

    [SerializeField] private Vector3 target;

    [SerializeField] private GameObject prefabBullet;

    [SerializeField] private Transform pointShoot;
    private float timerWait;
    private float timerReload;

    
    public override void UpdateShoot(Vector3 targetPosition)
    {
        base.UpdateShoot(targetPosition);

        target = targetPosition;
        
        timerWait += Time.fixedDeltaTime;
        if (timerWait > timeWait)
        {
            timerWait = 0;
            //effect.gameObject.SetActive(true); 

            if (CanShoot())
            {
                effect.Play();
                ActionShoot();
                //Shoot
                currentBullet -= 1;
                if (!CanShoot())
                {
                    timerReload = 0;
                }
            }
        }
    }

    public override void EndShoot()
    {
        base.EndShoot();
        timerWait = 0;
    }

    public override bool CanShoot()
    {
        return currentBullet > 0;
    }

    public override void Reloading()
    {
        
        base.Reloading();
        if (currentBullet <= 0)
        {
            effect.Stop();
            timerReload += Time.fixedDeltaTime;

            if (timerReload > timeReload)
            {
                currentBullet = maxBullet;
            }
        }
    }

    public void ActionShoot()
    {
        GameObject temp = PoolManager.Spawn(prefabBullet);

        temp.transform.forward = (target - pointShoot.position).normalized;
        temp.transform.position = pointShoot.position;
        temp.SetActive(true);
        temp.transform.DOMove(target, .5f).OnComplete(delegate
        {
            PoolManager.Recycle(temp.GetInstanceID());
        });
    }
    
}
