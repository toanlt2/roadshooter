using System;
using System.Collections;
using System.Collections.Generic;
using Entitas;
using SR4BlackDev;
using UnityEngine;

namespace SR4BlackDev
{
    public class GameController : MonoBehaviour
    {
        private Systems gameSystem;
        private Systems fixedUpdateSystems;
        private void Awake()
        {
            Setup();
        }

        private void Setup()
        {
            gameSystem = new Feature("GameSystem").Add(new SystemCleanEntity());
            fixedUpdateSystems = new Feature("FixedSystem").Add(new SystemUpdateCar(Contexts.SharedInstance.game));
        }
    
        public void Update()
        {
            gameSystem.Execute();
        }

        public void FixedUpdate()
        {
            fixedUpdateSystems.Execute();
        }

        public void LateUpdate()
        {
            gameSystem.Cleanup();
        }
    }


}
