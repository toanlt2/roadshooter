using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEditor;
using UnityEngine;

namespace SR4BlackDev
{
    public class CharacterController : MonoBehaviour
{
    [SerializeField] private Transform playerTrans;
    [SerializeField] private Animator animator;

    [SerializeField] private string shotAnim;
    [SerializeField] private string endShot;
    [SerializeField] private string idleAnim;
    [SerializeField] private string jumpingUp;
    [SerializeField] private string jumpingDown;
    //Target
    [SerializeField] private Transform defaultTrans;
    [SerializeField] private Transform coverTrans;
    [SerializeField] private Transform aimTrans; // thay = target position 
    [SerializeField] private Transform coverAimTrans;
    
    //Preview
    [SerializeField] private Transform previewTrans;
    [SerializeField] private float rotationSpeed;
    [SerializeField] private Vector3 bufferRotate;
    private bool isShooting = false;
    //View Camera
//    [SerializeField] private Transform camera;
    [SerializeField] private Transform topview;
    [SerializeField] private Transform previewView;
    
    //Curve
    [SerializeField] private AnimationCurve curveJumping;
    [SerializeField] private AnimationCurve curveJumpDown;
    [SerializeField] private bool isPlay = false;
    
    //Current Weapon
    [SerializeField] private IWeapon currentWeapon;
    //Current Character
    [SerializeField] private ICharacter currentCharacter;
    public void BeforeStartGamePlay()
    {
        isPlay = false;
        Preview();
        animator.CrossFade(jumpingUp,0.25f,0);
        playerTrans.DOLocalMove(coverTrans.localPosition,1f).OnComplete(delegate
        {
            isPlay = true;
            animator.CrossFade(endShot,0.25f,0);
        }).SetEase(curveJumping);

        DOVirtual.DelayedCall(.15f, delegate { playerTrans.DORotate(Vector3.zero, .25f); });

        Transform camera = CameraController.GetCamera;
        camera.transform.parent = topview;

        camera.DOLocalMove(Vector3.zero, 1f);
        camera.DORotate(Vector3.zero + new Vector3(0,180f,0), 1f);

    }

    public void JumpDown()
    {
        playerTrans.localPosition = coverTrans.localPosition;
        animator.CrossFade(jumpingDown,0,0);
        playerTrans.DOLocalMove(previewTrans.localPosition, 0.25f).OnComplete(delegate
        {
            isPlay = false;
            
        }).SetEase(curveJumpDown);
        DOVirtual.DelayedCall(.3f, delegate
        {
            animator.CrossFade(idleAnim, 0.25f, 0);
        });
        
        playerTrans.DORotate(new Vector3(0,90f,0), .25f );
        
        Transform camera = CameraController.GetCamera;
        camera.transform.parent = previewView;
        camera.DOLocalMove(Vector3.zero, 1f);
        //camera.forward = previewView.forward;
        camera.DORotate(previewView.localRotation.eulerAngles, 1f);
    }
    private void Start()
    {
        JumpDown();
    }

    // Update is called once per frame
    void Update()
    {
        if (!isPlay) return;
        if (!currentWeapon) return;
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if(currentWeapon.CanShoot())
                Shoot();
        }
        else
        {
            if (Input.GetKeyUp(KeyCode.Space))
            {
                Cover(); 
            }
        }
        
    }

    private void FixedUpdate()
    {
        if (!isPlay) return;
        if (!currentWeapon) return;
        

            if (!currentWeapon.CanShoot())
            {
                currentWeapon.Reloading();
                
                Vector3 directionToTarget = coverAimTrans.position - playerTrans.position;
                directionToTarget.y = 0f; 
                Quaternion targetRotation = Quaternion.LookRotation(directionToTarget);
                playerTrans.rotation = Quaternion.RotateTowards(playerTrans.rotation, targetRotation, Time.fixedDeltaTime * rotationSpeed);
                currentWeapon.EndShoot();
                
                Cover();
                return;
            }
        
        
        if (isShooting )
        {
            Vector3 directionToTarget = (aimTrans.position - playerTrans.position).normalized;
            directionToTarget.y = 0;
            directionToTarget = directionToTarget + bufferRotate ;
            Quaternion targetRotation = Quaternion.LookRotation(directionToTarget);
            playerTrans.rotation = Quaternion.RotateTowards(playerTrans.rotation, targetRotation, Time.fixedDeltaTime * rotationSpeed);

            currentWeapon.UpdateShoot(aimTrans.position);

        }
        else
        {
            Vector3 directionToTarget = coverAimTrans.position - playerTrans.position;
            directionToTarget.y = 0f; 
            Quaternion targetRotation = Quaternion.LookRotation(directionToTarget);
            playerTrans.rotation = Quaternion.RotateTowards(playerTrans.rotation, targetRotation, Time.fixedDeltaTime * rotationSpeed);

            currentWeapon.EndShoot();
            
        }
    }
    
    

    public void Shoot()
    {
        animator.CrossFade(shotAnim,0.25f,0);
        playerTrans.DOLocalMove(defaultTrans.localPosition,0.25f);
        isShooting = true;
    }

    public void Cover()
    {
        animator.CrossFade(endShot,0.25f,0);
        playerTrans.DOLocalMove(coverTrans.localPosition,0.25f);
        isShooting = false;

    }

    public void Preview()
    {

        animator.CrossFade(idleAnim,0.25f,0);
        playerTrans.localPosition = previewTrans.localPosition;
        //playerTrans.DOLocalMove(previewTrans.localPosition,0.25f);
        playerTrans.forward = previewTrans.forward;
        
        Transform camera = CameraController.GetCamera;
        camera.transform.parent = previewView;
        camera.transform.localPosition = Vector3.zero;
        camera.forward = previewView.forward;
    }
}
[CustomEditor(typeof(CharacterController))] 
public class CharacterControllerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        CharacterController yourClass = (CharacterController)target;

        if (GUILayout.Button("Play"))
        {
            yourClass.BeforeStartGamePlay();
        }

        if (GUILayout.Button("JumpDown"))
        {
            yourClass.JumpDown();
        }
    }
}

}
